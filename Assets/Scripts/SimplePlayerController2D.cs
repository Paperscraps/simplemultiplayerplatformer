﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SimplePlayerController2D : MonoBehaviour
{
    public float moveForce = 350f;
    public float maxSpeed = 5f;
    public float jumpForce = 750f;
    public float shootForce = 250f;

    public float transformCheckRadius = .025f;
    public LayerMask collisionMask;
    public Transform[] groundChecks;
    public Transform body;
    public Transform armPivot;

    bool canJump;
    bool facingRight = true;

    Rigidbody2D rb2d;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        canJump = TransformCollisionCheck(groundChecks, collisionMask);
    }

    public void Move(float h, float v, bool j, bool s)
    {
        if (h * rb2d.velocity.x < maxSpeed)
        {
            rb2d.AddForce(Vector2.right * h * moveForce);
        }

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
        {
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
        }

        if (h > 0 && !facingRight)
        {
            Flip();
        }
        else if (h < 0 && facingRight)
        {
            Flip();
        }

        if (j && canJump)
        {
            rb2d.AddForce(Vector2.up * jumpForce);
        }

        if (s)
        {
            float angleInRad = Mathf.Deg2Rad * armPivot.rotation.eulerAngles.z;
            Vector2 unitVector = new Vector2(Mathf.Cos(angleInRad), Mathf.Sin(angleInRad));
            rb2d.AddForce(-unitVector * shootForce);
        }
    }

    void Flip()
    {
        facingRight = !facingRight;
        Vector3 tempScale = body.localScale;
        tempScale.x *= -1;
        body.localScale = tempScale;
    }

    bool TransformCollisionCheck(Transform[] transforms, LayerMask mask)
    {
        for (int i = 0; i < transforms.Length; i++)
        {
            if (Physics2D.OverlapCircle(transforms[i].position, transformCheckRadius, mask))
            {
                return true;
            }
        }
        return false;
    }
}
