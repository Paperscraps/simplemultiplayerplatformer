﻿using UnityEngine;

[RequireComponent(typeof(SimplePlayerController2D))]
public class PlayerInput : MonoBehaviour
{
    public GameObject bullet;
    public Transform armPivot;
    public Transform hand;

    SimplePlayerController2D controller;

    bool jump, shoot;
    float horizontalMove, verticalMove;
    float shootDelay;
    float shootDelayMax = .12f;

    void Awake()
    {
        controller = GetComponent<SimplePlayerController2D>();
    }

    void Update()
    {
        if (shootDelay >= Mathf.Epsilon)
        {
            shootDelay -= Time.deltaTime;
        }

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
        }

        if (Input.GetMouseButton(0))
        {
            if (shootDelay < Mathf.Epsilon)
            {
                Shoot();
                shootDelay = shootDelayMax;
            }
        }
    }

    void FixedUpdate()
    {
        horizontalMove = Input.GetAxis("Horizontal");
        verticalMove = Input.GetAxis("Vertical");

        controller.Move(horizontalMove, verticalMove, jump, shoot);

        jump = shoot = false;
    }

    void Shoot()
    {
        shoot = true;

        if (PhotonNetwork.inRoom)
        {
            PhotonNetwork.Instantiate("Bullet", hand.position, armPivot.rotation, 0);
        }
        else
        {
            Instantiate(bullet, hand.position, armPivot.rotation);
        }
    }
}
