﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text textInput;
    public Text errorText;
    public Text connectionText;
    public GameObject mainPanel;
    public GameObject playerPrefab;

    public GameObject boundsGO;
    public Transform spawn;
    public static BoxCollider2D Bounds;

    GameObject tempPlayer;

    void Awake()
    {
        Bounds = boundsGO.GetComponent<BoxCollider2D>();
    }

    void Start()
    {
        //PhotonNetwork.logLevel = PhotonLogLevel.Full;
    }

    void Update()
    {
        connectionText.text = PhotonNetwork.connectionStateDetailed.ToString();
    }

    public void OnLocalClick()
    {
        if (textInput.text.Length == 0)
        {
            errorText.text = "Please enter a valid name!";
            return;
        }

        mainPanel.SetActive(false);
        tempPlayer = Instantiate<GameObject>(playerPrefab);
        GameManager.EnablePlayer(tempPlayer);

        tempPlayer.GetComponentInChildren<TextMesh>().text = textInput.text;
        tempPlayer.GetComponent<PlayerPhotonView>().enabled = false;
    }

    public void OnMultiplayerClick()
    {
        if (textInput.text.Length == 0)
        {
            errorText.text = "Please enter a valid name!";
            return;
        }

        mainPanel.SetActive(false);
        PhotonNetwork.ConnectUsingSettings("0.1");
    }

    void OnJoinedLobby()
    {
        RoomOptions ro = new RoomOptions() { isVisible = true, maxPlayers = 10 };
        PhotonNetwork.JoinOrCreateRoom("MainRoom", ro, TypedLobby.Default);
    }

    void OnJoinedRoom()
    {
        StartCoroutine("SpawnPlayer");
    }

    IEnumerator SpawnPlayer()
    {
        yield return new WaitForSeconds(2f);
        tempPlayer = PhotonNetwork.Instantiate("Player", spawn.position, Quaternion.identity, 0);
        tempPlayer.GetComponentInChildren<TextMesh>().text = textInput.text;
    }

    public static void EnablePlayer(GameObject player)
    {
        player.GetComponent<PlayerInput>().enabled = true;
        player.GetComponent<SimplePlayerController2D>().enabled = true;
        player.GetComponent<Rigidbody2D>().gravityScale = 1f;
        player.GetComponent<CameraController>().enabled = true;
        player.GetComponent<CameraController>().SendMessage("UpdateBounds", GameManager.Bounds);
        player.GetComponentInChildren<ArmRotation>().enabled = true;
    }
}
