﻿using UnityEngine;
using System.Collections;

public class PlayerPhotonView : Photon.MonoBehaviour 
{
    public Transform body;
    public Transform armPivot; 

    Vector3 syncStartPosition;
    Vector3 syncEndPosition;
    Vector3 localScale;
    Quaternion armRotation;

    float lastSynchronizationTime = 0f;
    float syncDelay = 0f;
    float syncTime = 0f;

    bool notMyPlayer = true;
    bool gotFirstUpdate; 

    Rigidbody2D rb2d;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Start () 
    {
        armRotation = Quaternion.identity;

        if (photonView.isMine)
        {
            notMyPlayer = false;            
            GameManager.EnablePlayer(this.gameObject);

            photonView.RPC("UpdateName", PhotonTargets.OthersBuffered, GetComponentInChildren<TextMesh>().text);
        }
    }

    void Update()
    {
        if (notMyPlayer)
        {            
            syncTime += Time.deltaTime;

            body.localScale = localScale;
            armPivot.rotation = Quaternion.Lerp(armPivot.rotation, armRotation, syncTime / syncDelay);
            transform.position = Vector3.Lerp(syncStartPosition, syncEndPosition, syncTime / syncDelay);
        }
    }
    
    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext((Vector2)rb2d.position);
            stream.SendNext((Vector2)rb2d.velocity);
            stream.SendNext((Vector3)body.localScale);
            stream.SendNext((Quaternion)armPivot.rotation);
        }
        else
        {
            Vector2 syncPosition = (Vector2)stream.ReceiveNext();
            Vector2 syncVelocity = (Vector2)stream.ReceiveNext(); 
            localScale = (Vector3)stream.ReceiveNext();
            armRotation = (Quaternion)stream.ReceiveNext();            

            syncTime = 0f;
            syncDelay = Time.time - lastSynchronizationTime;
            lastSynchronizationTime = Time.time;

            syncEndPosition = syncPosition + syncVelocity * syncDelay;
            syncStartPosition = transform.position;

            if (gotFirstUpdate == false)
            {
                transform.position = syncEndPosition;
                body.localScale = localScale;
                armPivot.rotation = armRotation;
                gotFirstUpdate = true;
            }
        }
    }

    [RPC]
    void UpdateName(string s)
    {
        GetComponentInChildren<TextMesh>().text = s;
    }
}