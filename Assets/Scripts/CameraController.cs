﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public Vector2
        Margin,
        Smoothing;

    public BoxCollider2D Bounds;

    private Vector3
        _min,
        _max;

    public bool IsFollowing { get; set; }

    public void Start()
    {
        _min = Bounds.bounds.min;
        _max = Bounds.bounds.max;
        IsFollowing = true;
    }

    public void Update()
    {
        var x = Camera.main.transform.position.x;
        var y = Camera.main.transform.position.y;

        if (IsFollowing)
        {

            x = transform.position.x;
            y = transform.position.y;

            //if (Mathf.Abs(x - transform.position.x) > Margin.x)
            //    x = Mathf.Lerp(x, transform.position.x, Smoothing.x * Time.deltaTime);

            //if (Mathf.Abs(y - transform.position.y) > Margin.y)
            //    y = Mathf.Lerp(y, transform.position.y, Smoothing.y * Time.deltaTime);            
        }        

        var cameraHalfWidth = Camera.main.orthographicSize * ((float)Screen.width / Screen.height);

        x = Mathf.Clamp(x, _min.x + cameraHalfWidth, _max.x - cameraHalfWidth);
        y = Mathf.Clamp(y, _min.y + Camera.main.orthographicSize, _max.y - Camera.main.orthographicSize);

        Camera.main.transform.position = new Vector3(x, y, Camera.main.transform.position.z);
    }

    void UpdateBounds(BoxCollider2D b)
    {
        Bounds = b;
    }
}
