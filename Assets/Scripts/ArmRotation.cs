﻿using UnityEngine;
using System.Collections;

public class ArmRotation : MonoBehaviour
{
    public int RotationOffset = 90;

    void Update()
    {
        var difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        difference.Normalize();

        var rotationAngle = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rotationAngle + RotationOffset);
    }
}