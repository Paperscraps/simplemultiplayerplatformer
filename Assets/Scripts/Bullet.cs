﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletForce = 650f;
    Rigidbody2D rb2d;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        float angleInRad = Mathf.Deg2Rad * transform.rotation.eulerAngles.z;
        Vector2 unitVector = new Vector2(Mathf.Cos(angleInRad), Mathf.Sin(angleInRad));
        rb2d.AddForce(unitVector * bulletForce);
        Destroy(this.gameObject, 10f);
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        Destroy(this.gameObject);
    }
}
